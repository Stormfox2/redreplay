/*
 *
 *  * @authors Stormfox2
 *  * Created in 05.12.2018
 *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *
 *
 */

package de.redmc;

import de.redmc.item.ItemCreator;
import de.redmc.listener.JoinQuitEvent;
import de.redmc.mysql.AsyncMySQL;
import de.redmc.mysql.ColumnType;
import de.redmc.mysql.TableManager;
import de.redmc.replay.ReplayManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class RedReplay extends JavaPlugin {
    //ConsoleCommandSender
    ConsoleCommandSender console;

    //MySQL
    AsyncMySQL asyncMySQL;
    TableManager tableManager;

    //ItemCreator
    ItemCreator itemCreator;
    ArrayList<String> lore;
    ItemStack lobbyItem;

    //Replay
    ReplayManager replayManager;

    @Override
    public void onEnable() {
        init();
    }

    @Override
    public void onDisable() {
        if(!getServer().getServerName().contains("Lobby") || !getServer().getServerName().contains("SilentLobby") || !getServer().getServerName().contains("Replay") )
             asyncMySQL.update("INSERT INTO ReplayNames VALUES ('test', '" + getServer().getServerName() + "'");
    }

    private void init() {
        //ConsoleCommandSender
        console = Bukkit.getConsoleSender();

        //MySQL
        asyncMySQL = new AsyncMySQL(this, "127.0.0.1", 3306, "replay", "KWtghUgljdsBNbxv", "replay");
        tableManager = new TableManager(this);

        //Items
        itemCreator = new ItemCreator();
        lore = new ArrayList<>();
        lore.add("Schaue dir deine letzten Spiele an!");
        lobbyItem = itemCreator.createItem("Replay", Material.PAPER, 1, lore);

        //Listener
        this.getServer().getPluginManager().registerEvents(new JoinQuitEvent(this), this);

        //Replay
        replayManager = new ReplayManager(this);

        createTable();
    }

    private void createTable() {

    }

    public ConsoleCommandSender getConsole(){
        return console;
    }

    public void sendConsoleMessage(String string) {
        console.sendMessage(string);
    }

    public AsyncMySQL getMySQL(){
        return asyncMySQL;
    }

    public ItemStack getLobbyItem(){
        return lobbyItem;
    }

    public ReplayManager getReplayManager(){
        return replayManager;
    }
}
