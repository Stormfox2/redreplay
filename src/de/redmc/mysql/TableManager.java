package de.redmc.mysql;


import de.redmc.RedReplay;

public class TableManager {

    private final RedReplay plugin;
    private final AsyncMySQL asyncMySQL;

    public TableManager(RedReplay plugin) {
        this.plugin = plugin;
        this.asyncMySQL = plugin.getMySQL();
    }

    public void createTable(String tableName) {
        asyncMySQL.update("CREATE TABLE IF NOT EXIST '" + tableName + "'");
    }

    public void addColumn(String tableName, String column, ColumnType columnType, boolean first) {
        if (columnType.equals(ColumnType.VARCHAR)) {
            if (first)
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' VARCHAR(150) FIRST");
            else
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' VARCHAR(150)");
        } else if (columnType.equals(ColumnType.INTEGER)) {
            if (first)
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' INTEGER FIRST");
            else
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' INTEGER");
        } else if (columnType.equals(ColumnType.DATETIME)) {
            if (first)
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DATETIME FIRST");
            else
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DATETIME");
        } else if (columnType.equals(ColumnType.DOUBLE)) {
            if (first)
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DOUBLE FIRST");
            else
                asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DOUBLE");
        }
    }

    public void addColumnAfter(String tableName, String column, ColumnType columnType, String after) {
        if (columnType.equals(ColumnType.VARCHAR)) {
            asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' VARCHAR(150) AFTER '" + after + "'");
        } else if (columnType.equals(ColumnType.INTEGER)) {
            asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' INTEGER AFTER '" + after + "'");
        } else if (columnType.equals(ColumnType.DATETIME)) {
            asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DATETIME AFTER '" + after + "'");
        } else if (columnType.equals(ColumnType.DOUBLE)) {
            asyncMySQL.update("ALTER TABLE '" + tableName + "' ADD '" + column + "' DOUBLE AFTER '" + after + "'");
        }
    }
}
