/*
 *
 *  *
 *  *  * @authors Stormfox2
 *  *  * Created in 05.12.2018
 *  *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *  *
 *  *
 *
 */

package de.redmc.mysql;

public enum ColumnType {
    VARCHAR, INTEGER, DATETIME, DOUBLE
}
