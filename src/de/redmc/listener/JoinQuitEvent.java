/*
 *
 *  *
 *  *  * @authors Stormfox2
 *  *  * Created in 05.12.2018
 *  *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *  *
 *  *
 *
 */

package de.redmc.listener;

import de.redmc.RedReplay;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitEvent implements Listener {
    RedReplay plugin;

    public JoinQuitEvent(RedReplay plugin){
        this.plugin = plugin;
        System.out.println("§aJoinEvent loaded!");
    }

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent e){
        if(Bukkit.getServerName().contains("Lobby")){ } else {
            e.getPlayer().getInventory().setItem(19, plugin.getLobbyItem());
            plugin.getReplayManager().addReplayPlayer(e.getPlayer());
            e.getPlayer().sendMessage("Loading....");
        }
    }

    @EventHandler
    public void onQuitEvent(PlayerQuitEvent e){
        if(!Bukkit.getServerName().contains("Lobby")){ } else {
            plugin.getReplayManager().removeReplayPlayer(e.getPlayer());
        }
    }
}
