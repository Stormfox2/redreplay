/*
 *
 *  *
 *  *  * @authors Stormfox2
 *  *  * Created in 05.12.2018
 *  *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *  *
 *  *
 *
 */

package de.redmc.replay;

import com.sun.org.apache.xpath.internal.operations.Bool;
import de.redmc.RedReplay;
import de.redmc.mysql.AsyncMySQL;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class ReplayPlayer {
    private String uniqueID;
    private RedReplay plugin;
    AsyncMySQL asyncMySQL;
    String replayHistory;
    ArrayList<Replay> replays;

    public ReplayPlayer(String uniqueId, RedReplay plugin) {
        this.plugin = plugin;
        this.uniqueID = uniqueId;

        asyncMySQL = plugin.getMySQL();

        asyncMySQL.query("SELECT * FROM ReplayHistory WHERE UUID=" + uniqueId , resultSet -> {
            try {
                if (resultSet.next()){
                    replayHistory = resultSet.getString(2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        plugin.sendConsoleMessage("Fetched Mysql!");

        System.out.println(replayHistory);

        for(String replayID : replayHistory.split(",")){
            replays.add(new Replay(replayID, plugin) );
            plugin.sendConsoleMessage("Loaded " + replayID + "!");
        }


        plugin.sendConsoleMessage("Player loaded!");
    }

    public ArrayList<Replay> getReplays() {
        return replays;
    }

    public String getUniqueID() {
        return uniqueID;
    }
}
