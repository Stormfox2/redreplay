/*
 *
 *  *
 *  *  * @authors Stormfox2
 *  *  * Created in 05.12.2018
 *  *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *  *
 *  *
 *
 */

package de.redmc.replay;

import de.redmc.RedReplay;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ReplayManager {
    private RedReplay plugin;

    private ArrayList<ReplayPlayer> playerList;

    public ReplayManager(RedReplay plugin){
        this.plugin = plugin;
    }

    public void addReplayPlayer(Player player){
        playerList.add(new ReplayPlayer(player.getUniqueId().toString(), plugin));
    }

    public void removeReplayPlayer(Player player) {
        playerList.remove(getReplayPlayerByName(player.getUniqueId().toString()));
    }

    public ReplayPlayer getReplayPlayerByName(String uuid) {
        for(ReplayPlayer rPlayer : playerList){
            if(rPlayer.getUniqueID().equalsIgnoreCase(uuid))
                return rPlayer;

        }

        return null;
    }
}
