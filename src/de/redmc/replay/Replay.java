/*
 *
 *  *
 *  *  * @authors Stormfox2
 *  *  * Created in 05.12.2018
 *  *  * Copyright (c) 2018 by redmc.de. All rights reserved.
 *  *  *
 *  *
 *
 */

package de.redmc.replay;

import de.redmc.RedReplay;

import java.sql.SQLException;
import java.util.Date;

public class Replay {
    RedReplay plugin;
    String replayID;
    String gameName;
    Date recordDate;
    int duration;


    public Replay(String replayID, RedReplay plugin) {
        this.plugin = plugin;
        this.replayID = replayID;



        plugin.getMySQL().query("SELECT * FROM ReplayIndex WHERE REPLAYID='" + replayID + "'", resultSet -> {
            try {
                if(resultSet.next()) {
                    recordDate.setTime(resultSet.getInt(6));
                    duration = resultSet.getInt(7) / 60;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } );

        plugin.getMySQL().query("SELECT * FROM ReplayNames WHERE ReplayID='" + replayID + "'", resultSet -> {
            try {
                if(resultSet.next()) {
                    gameName = resultSet.getString(2).split("-")[0];
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } );

    }
}
